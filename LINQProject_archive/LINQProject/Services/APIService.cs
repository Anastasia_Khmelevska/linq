﻿using System;
using System.Linq;
using System.Collections.Generic;
using LINQProject.Models;
using LINQProject.APIClients;

namespace LINQProject.Services
{
    class APIService
    {
        #region Properties
        private APIClient apiClient = new APIClient();
        private APIClient APIClient =>
            apiClient;
        #endregion
        #region Containers
        private IEnumerable<Project> projects;
        private IEnumerable<Project> Projects =>
            projects ?? (projects = GetProjects());

        private IEnumerable<Task> tasks;
        private IEnumerable<Task> Tasks =>
            tasks ?? (tasks = GetTasks());

        private IEnumerable<AuthorPerformer> authorPerformers;
        private IEnumerable<AuthorPerformer> AuthorPerformers =>
            authorPerformers ?? (authorPerformers = GetAuthorPerformers());

        private IEnumerable<Team> teams;
        private IEnumerable<Team> Teams =>
            teams ?? (teams = GetTeams());

        private IEnumerable<TaskState> taskStates;
        private IEnumerable<TaskState> TaskStates =>
            taskStates ?? (taskStates = GetTaskStates());
        #endregion
        #region Public Methods
        public IEnumerable<Project> GetProjects()
        {
            if (projects != null) return Projects;
            IEnumerable<Project> _projects;
            try
            {
                _projects = APIClient.GetProjects()
                    .Select(project => new Project
                    {
                        Id = project.Id,
                        Name = project.Name,
                        CreatedAt = project.Created_At,
                        Deadline = project.Deadline,
                        Tasks = Tasks.Where(task => task.ProjectId == project.Id),
                        Author = AuthorPerformers.Single(author => author.Id == project.Author_Id),
                        Team = Teams.Single(team => team.Id == project.Team_Id)
                    });
            }
            catch (Exception)
            {
                throw;
            }

            projects = _projects;
            return _projects;
        }
        public IEnumerable<Task> GetTasks()
        {
            if (tasks != null) return Tasks;
            IEnumerable<Task> _tasks;
            try
            {
                _tasks = APIClient.GetTasks()
                    .Select(task => new Task
                    {
                        Id = task.Id,
                        Name = task.Name,
                        Description = task.Description,
                        CreatedAt = task.Created_At,
                        FinishedAt = task.Finished_At,
                        ProjectId = task.Project_Id,
                        State = TaskStates.Single(state => state.Id == task.State),
                        Performer = AuthorPerformers.Single(performer => performer.Id == task.Performer_Id)
                    });
            }
            catch (Exception)
            {
                throw;
            }

            tasks = _tasks;
            return _tasks;
        }
        public IEnumerable<AuthorPerformer> GetAuthorPerformers()
        {
            if (authorPerformers != null) return AuthorPerformers;
            IEnumerable<AuthorPerformer> _authorPerformer;
            try
            {
                _authorPerformer = APIClient.GetUsers()
                    .Select(user => new AuthorPerformer {
                        Id = user.Id,
                        FirstName = user.First_Name,
                        LastName = user.Last_Name,
                        Email = user.Email,
                        Birthday = user.Birthday,
                        RegisteredAt = user.Registered_At,
                        TeamId = user.Team_Id ?? 0
                    });
            }
            catch (Exception)
            {
                throw;
            }

            authorPerformers = _authorPerformer;
            return _authorPerformer;
        }
        public IEnumerable<Team> GetTeams()
        {
            if (teams != null) return Teams;
            IEnumerable<Team> _teams;
            try
            {
                _teams = APIClient.GetTeams()
                    .Select(team => new Team {
                        Id = team.Id,
                        Name = team.Name,
                        CreatedAt = team.Created_At
                    });
            }
            catch (Exception)
            {
                throw;
            }

            teams = _teams;
            return _teams;
        }
        public IEnumerable<TaskState> GetTaskStates()
        {
            if (taskStates != null) return TaskStates;
            IEnumerable<TaskState> _taskStates;
            try
            {
                _taskStates = APIClient.GeTaskStates()
                    .Select(task => new TaskState {
                        Id = task.Id,
                        Value = task.Value
                    });
            }
            catch(Exception)
            {
                throw;
            }

            taskStates = _taskStates;
            return _taskStates;
        }
        #endregion
    }
}
