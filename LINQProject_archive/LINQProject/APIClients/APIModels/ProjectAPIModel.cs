﻿using System;

namespace LINQProject.APIClients.APIModels
{
    public class ProjectAPIModel : BaseModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Created_At { get; set; }
        public DateTime Deadline { get; set; }
        public long Author_Id { get; set; }
        public long Team_Id { get; set; }
    }
}
