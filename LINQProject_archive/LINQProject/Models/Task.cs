﻿using System;

namespace LINQProject.Models
{
    public class Task : Entity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime FinishedAt { get; set; }
        public long ProjectId { get; set; }
        public TaskState State { get; set; }
        public AuthorPerformer Performer { get; set; }

        public override string ToString() =>
            "Name: " + Name + " | Description: " + Description + " | Created at: " + CreatedAt.ToString() + " | Finished at: " + FinishedAt.ToString() + " | Project Id: " + ProjectId;
    }
}
